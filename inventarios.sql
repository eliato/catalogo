﻿# Host: localhost  (Version 5.7.15-log)
# Date: 2018-07-16 18:29:07
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "cat_categoria"
#

CREATE TABLE `cat_categoria` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_nombre` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Data for table "cat_categoria"
#

INSERT INTO `cat_categoria` VALUES (1,'GAS TOMZA'),(2,'CILINDROS TOMZA'),(3,'GAS UNIGAS'),(4,'CILINDROS UNIGAS'),(5,'CILINDROS TROPIGAS'),(6,'CILINDROS Z GAS');

#
# Structure for table "cd_cedi"
#

CREATE TABLE `cd_cedi` (
  `cedi_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_nombre` varchar(25) DEFAULT NULL,
  `cd_estado` int(1) DEFAULT '0' COMMENT '0=activo, 1=inactivo',
  `cd_fmodificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cedi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

#
# Data for table "cd_cedi"
#

INSERT INTO `cd_cedi` VALUES (1,'CHALCHUAPA',0,'2018-07-06 17:04:52'),(2,'SAN SALVADOR',0,'2018-07-06 17:04:53'),(3,'SANTA ANA',0,'2018-07-06 17:04:53'),(4,'SAN MIGUEL',0,'2018-07-06 17:04:53'),(5,'SOYAPANGO',0,'2018-07-06 17:04:54'),(6,'ZACATECOLUCA',0,'2018-07-06 17:04:54'),(7,'USULUTAN',0,'2018-07-06 17:04:55'),(8,'SONSONATE',0,'2018-07-06 17:04:56');

#
# Structure for table "can_canal"
#

CREATE TABLE `can_canal` (
  `canal_id` int(11) NOT NULL AUTO_INCREMENT,
  `can_id_cedi` int(2) DEFAULT NULL,
  `can_nombre` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`canal_id`),
  KEY `can_cd` (`can_id_cedi`),
  CONSTRAINT `can_cd` FOREIGN KEY (`can_id_cedi`) REFERENCES `cd_cedi` (`cedi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "can_canal"
#

INSERT INTO `can_canal` VALUES (1,2,'VIP'),(2,2,'TIENDA PROPIA'),(3,2,'MERCANTIL');

#
# Structure for table "pro_producto"
#

CREATE TABLE `pro_producto` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_cod` char(6) DEFAULT NULL,
  `pro_nombre` varchar(30) DEFAULT NULL,
  `pro_precio` decimal(7,2) DEFAULT NULL,
  `pro_galon` decimal(7,3) DEFAULT NULL,
  `pro_id_cat` int(1) DEFAULT NULL,
  `pro_precio_sub` decimal(7,2) DEFAULT NULL,
  `fmodificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pro_id`),
  KEY `pro_cat` (`pro_id_cat`),
  CONSTRAINT `pro_cat` FOREIGN KEY (`pro_id_cat`) REFERENCES `cat_categoria` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

#
# Data for table "pro_producto"
#

INSERT INTO `pro_producto` VALUES (1,'GT10','Gas 10 Lbs Tomza',5.02,2.388,1,5.02,'2018-07-11 19:07:11'),(2,'GT20','Gas 20 Lbs Tomza',9.78,4.776,1,6.00,'2018-07-11 19:07:37'),(3,'GT25','Gas 25 Lbs Tomza',12.14,5.970,1,6.00,'2018-07-11 19:07:37'),(4,'GT35','Gas 35 Lbs Tomza',16.90,8.358,1,6.00,'2018-07-11 19:07:38'),(5,'GT60','Gas 60 Lbs Tomza',29.34,14.328,1,0.00,'2018-07-11 19:07:40'),(6,'GT100','Gas 100 Lbs Tomza',48.56,23.880,1,0.00,'2018-07-11 19:07:40'),(7,'CT10','Cilindro 10 Lbs Tomza',0.00,0.000,2,0.00,'2018-07-11 22:51:20'),(8,'CT20','Cilindro 20 Lbs Tomza',0.00,0.000,2,0.00,'2018-07-11 22:52:02'),(9,'CT25','Cilindro 25 Lbs Tomza',0.00,0.000,2,0.00,'2018-07-11 22:52:20'),(10,'CT35','Cilindro 35 Lbs Tomza',0.00,0.000,2,0.00,'2018-07-11 22:52:45'),(11,'CT60','Cilindro 60 Lbs Tomza',0.00,0.000,2,0.00,'2018-07-11 22:53:12'),(12,'CT100','Cilindro 100 Lbs Tomza',0.00,0.000,2,0.00,'2018-07-11 22:53:36'),(13,'GU20','Gas 20 Lbs Unigas',9.78,4.776,3,0.00,'2018-07-11 22:56:19'),(14,'GU25','Gas 25 Lbs Unigas',12.14,5.970,3,0.00,'2018-07-11 23:05:17'),(15,'GU35','Gas 35 Lbs Unigas',16.90,8.358,3,0.00,'2018-07-11 22:57:16'),(16,'GU60','Gas 60 Lbs Unigas ',29.34,14.328,3,0.00,'2018-07-11 22:57:51'),(17,'GU100','Gas 100 Lbs Unigas',48.56,23.880,3,0.00,'2018-07-11 22:58:22'),(18,'CU20','Cilindro 20 Lbs Unigas',0.00,0.000,4,0.00,'2018-07-12 11:45:59'),(19,'CU25','Cilindro 25 Lbs Unigas',0.00,0.000,4,0.00,'2018-07-12 11:46:24'),(20,'CU35','Cilindro 35 Lbs Unigas',0.00,0.000,4,0.00,'2018-07-12 11:46:59'),(21,'CU60','Cilindro 60 Lbs Unigas',0.00,0.000,4,0.00,'2018-07-12 11:47:22'),(22,'CU100','Cilindro 100 Lbs Unigas',0.00,0.000,4,0.00,'2018-07-12 11:47:41'),(23,'CTP10','Cilindro 10 Lbs Tropigas',0.00,0.000,5,0.00,'2018-07-12 11:56:46'),(24,'CTP20','Cilindro 20 Lbs Tropigas',0.00,0.000,5,0.00,'2018-07-12 11:58:14'),(25,'CTP25','Cilindro 25 Lbs Tropigas',0.00,0.000,5,0.00,'2018-07-12 11:58:45'),(26,'CTP35','Cilindro 35 Lbs Tropigas',0.00,0.000,5,0.00,'2018-07-12 11:59:04'),(27,'CTP60','Cilindro 60 Lbs Tropigas',0.00,0.000,5,0.00,'2018-07-12 11:59:39'),(28,'CTP100','Cilindro 100 Lbs Tropigas',0.00,0.000,5,0.00,'2018-07-12 12:10:24'),(29,'CZ10','Cilindro 10 Lbs Zgas',0.00,0.000,6,0.00,'2018-07-12 12:01:24'),(30,'CZ20','Cilindro 20 Lbs Zgas',0.00,0.000,6,0.00,'2018-07-12 12:01:05'),(31,'CZ25','Cilindro 25 Lbs Zgas',0.00,0.000,6,0.00,'2018-07-12 12:01:43'),(32,'CZ35','Cilindro 35 Lbs Zgas',0.00,0.000,6,0.00,'2018-07-12 12:02:15'),(33,'CZ60','Cilindro 60 Lbs Zgas',0.00,0.000,6,0.00,'2018-07-12 12:02:55'),(34,'CZ100','Cilindro 100 Lbs Zgas',0.00,0.000,6,0.00,'2018-07-12 12:03:19');

#
# Structure for table "roles"
#

CREATE TABLE `roles` (
  `idrol` int(2) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `menu` varchar(50) NOT NULL,
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Data for table "roles"
#

INSERT INTO `roles` VALUES (1,'Administrador','superusuario.php'),(2,'Planillas','planillas.php'),(3,'Finanzas','finanzas.php'),(4,'Conta','conta.php');

#
# Structure for table "sup_supervisor"
#

CREATE TABLE `sup_supervisor` (
  `sup_id` int(11) NOT NULL AUTO_INCREMENT,
  `sup_nombre` varchar(35) DEFAULT NULL,
  `sup_id_canal` int(2) DEFAULT NULL,
  `sup_estado` int(1) DEFAULT '0' COMMENT '0=activo, 1=inactivo',
  PRIMARY KEY (`sup_id`),
  KEY `sup_can` (`sup_id_canal`),
  CONSTRAINT `sup_can` FOREIGN KEY (`sup_id_canal`) REFERENCES `can_canal` (`canal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Data for table "sup_supervisor"
#

INSERT INTO `sup_supervisor` VALUES (1,'EDWIN ANCHETA',2,0),(2,'DAVID',1,0),(3,'ELEZAR MERCEDES',2,0),(4,'VICTOR PEREZ',2,0);

#
# Structure for table "tie_tienda"
#

CREATE TABLE `tie_tienda` (
  `tie_id` int(11) NOT NULL AUTO_INCREMENT,
  `tie_telefono` char(9) DEFAULT NULL,
  `tie_nombre` varchar(50) DEFAULT NULL,
  `tie_id_sup` int(2) DEFAULT NULL,
  `tie_codigo` char(5) DEFAULT NULL,
  `tie_estado` int(1) DEFAULT '0' COMMENT '0=activo, 1=inactivo',
  `tie_fmodificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tie_id`),
  KEY `tie_sup` (`tie_id_sup`),
  CONSTRAINT `tie_sup` FOREIGN KEY (`tie_id_sup`) REFERENCES `sup_supervisor` (`sup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Data for table "tie_tienda"
#

INSERT INTO `tie_tienda` VALUES (1,'77419165','ELIAS TORRES',2,'ET01',0,'2018-07-07 11:59:50'),(2,'77418906','RCV',1,'RCV01',0,'2018-07-11 21:40:44');

#
# Structure for table "inv_inventario"
#

CREATE TABLE `inv_inventario` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id_tie` int(3) DEFAULT NULL,
  `inv_id_pro` int(2) DEFAULT NULL,
  `inv_cant` int(4) DEFAULT NULL,
  `inv_fcreacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`inv_id`),
  KEY `inv_tie` (`inv_id_tie`),
  KEY `ivn_pro` (`inv_id_pro`),
  CONSTRAINT `inv_tie` FOREIGN KEY (`inv_id_tie`) REFERENCES `tie_tienda` (`tie_id`),
  CONSTRAINT `ivn_pro` FOREIGN KEY (`inv_id_pro`) REFERENCES `pro_producto` (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

#
# Data for table "inv_inventario"
#

INSERT INTO `inv_inventario` VALUES (1,1,1,0,'2018-07-12 13:53:22'),(2,1,2,0,'2018-07-12 13:53:22'),(3,1,3,0,'2018-07-12 13:53:22'),(4,1,4,0,'2018-07-12 13:53:22'),(5,1,5,0,'2018-07-12 13:53:22'),(6,1,6,0,'2018-07-12 13:53:22'),(7,1,7,0,'2018-07-12 13:53:22'),(8,1,8,0,'2018-07-12 13:53:22'),(9,1,9,0,'2018-07-12 13:53:22'),(10,1,10,0,'2018-07-12 13:53:22'),(11,1,11,0,'2018-07-12 13:53:22'),(12,1,12,0,'2018-07-12 13:53:22'),(13,1,13,0,'2018-07-12 13:53:22'),(14,1,14,0,'2018-07-12 13:53:22'),(15,1,15,0,'2018-07-12 13:53:22'),(16,1,16,0,'2018-07-12 13:53:22'),(17,1,17,0,'2018-07-12 13:53:22'),(18,1,18,0,'2018-07-12 13:53:22'),(19,1,19,0,'2018-07-12 13:53:22'),(20,1,20,0,'2018-07-12 13:53:22'),(21,1,21,0,'2018-07-12 13:53:22'),(22,1,22,0,'2018-07-12 13:53:22'),(23,1,23,0,'2018-07-12 13:53:22'),(24,1,24,0,'2018-07-12 13:53:22'),(25,1,25,0,'2018-07-12 13:53:22'),(26,1,26,0,'2018-07-12 13:53:22'),(27,1,27,0,'2018-07-12 13:53:23'),(28,1,28,0,'2018-07-12 13:53:23'),(29,1,29,0,'2018-07-12 13:53:23'),(30,1,30,0,'2018-07-12 13:53:23'),(31,1,31,0,'2018-07-12 13:53:23'),(32,1,32,0,'2018-07-12 13:53:23'),(33,1,33,0,'2018-07-12 13:53:23'),(34,1,34,0,'2018-07-12 13:53:23');

#
# Structure for table "usuarios"
#

CREATE TABLE `usuarios` (
  `iduser` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(75) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0=activo, 1=inactivo',
  `lastlogin` datetime NOT NULL,
  `idrol` int(2) NOT NULL DEFAULT '0' COMMENT 'codigo para amarrar el rol del usuario',
  PRIMARY KEY (`iduser`),
  KEY `idrol` (`idrol`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`idrol`) REFERENCES `roles` (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Data for table "usuarios"
#

INSERT INTO `usuarios` VALUES (2,'admin','21232f297a57a5a743894a0e4a801fc3','administrador','',0,'2018-07-16 17:08:04',1);

#
# Structure for table "vt_venta"
#

CREATE TABLE `vt_venta` (
  `vt_id` int(11) NOT NULL AUTO_INCREMENT,
  `vt_id_tie` int(2) DEFAULT NULL,
  `vt_id_pro` int(2) DEFAULT NULL,
  `vt_cantidad` int(3) DEFAULT NULL,
  `vt_monto_gas` decimal(7,2) DEFAULT '0.00',
  `vt_cant_subsidios` int(2) DEFAULT '0',
  `vt_monto_sub` decimal(7,2) DEFAULT '0.00',
  `vt_total` decimal(7,2) DEFAULT '0.00',
  `vt_galones` decimal(10,3) DEFAULT NULL,
  `vt_fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`vt_id`),
  KEY `vt_tie` (`vt_id_tie`),
  KEY `vt_pro` (`vt_id_pro`),
  CONSTRAINT `vt_pro` FOREIGN KEY (`vt_id_pro`) REFERENCES `pro_producto` (`pro_id`),
  CONSTRAINT `vt_tie` FOREIGN KEY (`vt_id_tie`) REFERENCES `tie_tienda` (`tie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

#
# Data for table "vt_venta"
#

INSERT INTO `vt_venta` VALUES (1,1,1,0,0.00,0,0.00,0.00,0.000,'2018-07-12 13:53:21'),(2,1,2,0,0.00,0,0.00,0.00,0.000,'2018-07-12 13:53:21'),(3,1,3,14,169.96,9,54.00,115.96,83.580,'2018-07-12 13:53:21'),(4,1,4,4,67.60,0,0.00,67.60,33.432,'2018-07-12 13:53:21'),(5,1,5,0,0.00,0,0.00,0.00,0.000,'2018-07-12 13:53:21'),(6,1,6,0,0.00,0,0.00,0.00,0.000,'2018-07-12 13:53:21'),(7,1,13,0,0.00,0,0.00,0.00,0.000,'2018-07-12 13:53:21'),(8,1,14,0,0.00,0,0.00,0.00,0.000,'2018-07-12 13:53:21'),(9,1,15,0,0.00,0,0.00,0.00,0.000,'2018-07-12 13:53:21'),(10,1,16,0,0.00,0,0.00,0.00,0.000,'2018-07-12 13:53:22'),(11,1,17,0,0.00,0,0.00,0.00,0.000,'2018-07-12 13:53:22');
