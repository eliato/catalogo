<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<head>

<!-- Basic Page Needs -->
<meta charset="utf-8">
<title>Catalogo Tomza</title>
<meta name="description" content="">
<meta name="author" content="Ansonika">

<!-- Favicons-->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
<link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="font-awesome/css/font-awesome.css" rel="stylesheet" >
<link href="css/socialize-bookmarks.css" rel="stylesheet">
<link href="js/fancybox/source/jquery.fancybox.css?v=2.1.4" rel="stylesheet">
<link href="check_radio/skins/square/aero.css" rel="stylesheet">

<!-- Toggle Switch -->
<link rel="stylesheet" href="css/jquery.switch.css">

<!-- Owl Carousel Assets -->
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- Google web font -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Jquery -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.8.22.min.js"></script>

<!-- Wizard-->
<script src="js/jquery.wizard.js"></script>

<!-- Radio and checkbox styles -->
<script src="check_radio/jquery.icheck.js"></script>

<!-- HTML5 and CSS3-in older browsers-->
<script src="js/modernizr.custom.17475.js"></script>

<!-- Support media queries for IE8 -->
<script src="js/respond.min.js"></script>


</head>

<body>

<section id="top-area">
       	
            <div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>Catalogo Tiendas</h1>                
                </div>
       		</div>
            </div>
 </section>   

<section class="container" id="main">

<!-- Start Survey container -->
<div id="survey_container">

    
	<form name="example-1" id="wrapped" action="validateLogin.php" method="POST">
		<div id="middle-wizard">
			<div class="step">
				<div class="row">
					
        <div class="col-md-4 col-md-offset-4" align="center">
					
            <div id="error" style="display:none;" >
              <button type="button" class="close" data-dismiss="alert">x</button>
            </div>
            
            <div id="error1" style="display:none;" >
              <button type="button" class="close" data-dismiss="alert">x</button>
            </div>
            <div id="error2" style="display:none;" >
             
            </div>
            <ul class="data-list" >

							<li>							
              <input class="form-control" type="text" min="0" name="username" id="username" max="99999999" maxlength="8" placeholder="Usuario" />
            </li>
            <li><input class="form-control" type="password" min="0" name="password" id="password" max="99999999" maxlength="8" placeholder="Password" /></li>
							<li> 
                <button type="submit" style='background-color: #008CBA; /* Green */
                  border: none;
                  color: white;
                  padding: 15px 32px;
                  text-align: center;
                  text-decoration: none;
                  display: inline-block;
                  font-size: 16px;  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);' name="forward">Ingresar 
                </button>
              </li>
						</ul>
					</div><!-- end col-md-6 -->
             
					
				</div><!-- end row -->
             
				                
			</div><!-- end step-->
            
		</div><!-- end middle-wizard -->	
        
	</form>    
</div><!-- end Survey container -->

</section><!-- end section main container -->

<?php 
include('foother.php');
 ?>
 <script type="text/javascript">
	

 $(document).ready(function(){
      $('#username').focus();
        $("#wrapped").submit(function() {
        if (!$("#username").val()) {
          $("#error").text("Ingrese Usuario").addClass("alert alert-danger").fadeIn("Slow");
          $('#username').focus();
          
          return false;
        } else if(!$("#password").val()) {
          $("#error").text("ingrese Contraseña").addClass("alert alert-success").fadeIn("Slow");
          $('#password').focus();
          return false;
        } else {
          return true;
        }
      });
    });

 
</script>
