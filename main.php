<?php 
include("include/menu.php");
include('header.php');

?>
<!-- MDBootstrap Datatables  -->
<script type="text/javascript" src="js/addons/datatables.min.js"></script>
<section class="container" id="main">

<!-- Start Survey container -->
<div id="survey_container">
   
	<form name="example-1" id="wrapped" action="registrar.php" method="POST">
		<div id="middle-wizard">		
			<div class="step row">
				<div class="col-md-12"> 
           <div class="btn-group">
   <button type="button" class="btn btn-info" onClick="add();">Agregar</button>
</div>
       <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">

  <thead>
 
    <tr>
       <th class="th-sm">Nombre
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Cedi
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
      <th class="th-sm">Accion
        <i class="fa fa-sort float-right" aria-hidden="true"></i>
      </th>
     
    </tr>
  </thead>

  <tbody>
      <?php
      if ($idrol==1) {
         $st="SELECT tie_nombre,cd_nombre,tie_id FROM tie_tienda
      JOIN sup_supervisor on tie_id_sup=sup_id      
      JOIN cd_cedi on sup_id_cedi=cedi_id ORDER BY cd_nombre ASC";      
      $rt=$connection->query($st);
      while($r = mysqli_fetch_assoc($rt)){
      
      ?>
      
    <tr>
      <td><? echo $r['tie_nombre'];?></td>
      <td><? echo $r['cd_nombre'];?></td>
      <td><div class="btn-group">
                <button type="button" class="btn btn-info" onClick="editar(<?=$r['tie_id'];?>);">Edit</button>
                <button type="button" class="btn btn-success" onClick="ver(<?=$r['tie_id'];?>);">Ver</button>

            </div>
        </td>
      
    </tr>   
    <?  
    }
  }else{ 
      $st="SELECT tie_nombre,cd_nombre,tie_id FROM tie_tienda
      JOIN sup_supervisor on tie_id_sup=sup_id      
      JOIN cd_cedi on sup_id_cedi=cedi_id WHERE sup_id_cedi='$idcedi' ORDER BY cedi_id";
      $rt=$connection->query($st);
      while($r = mysqli_fetch_assoc($rt)){
      
      ?>
      <tr>
      <td><? echo $r['tie_nombre'];?></td>
      <td><? echo $r['cd_nombre'];?></td>
      <td><div class="btn-group">
                <button type="button" class="btn btn-info" onClick="editar(<?=$r['tie_id'];?>);">Edit</button>
                <button type="button" class="btn btn-success" onClick="ver(<?=$r['tie_id'];?>);">Ver</button>

            </div>
        </td>
      
    </tr>

    <?
  }
}  
?>
</table>

				</div>

				
			
		</div><!-- end step -->
            
		</div><!-- end middle-wizard -->
		
        
	</form>
    
</div><!-- end Survey container -->

</section><!-- end section main container -->

<?php  

include('foother.php');
?>
<script >
$(document).ready(function () {
  $('#dtBasicExample').DataTable({"ordering": false});
  $('.dataTables_length').addClass('bs-select');

});

function editar(id)
{
 window.location="formulario.php?id="+id;

}
function ver(id)
{
 window.location="verformulario.php?id="+id;

}

function add(){
  window.location="agregar.php";
}

</script>