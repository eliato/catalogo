<?php 
include('adodb5/database.php');
$tie_id=$_POST['tie_id'];

//variables venta tomza
$vtf10=$_POST['vtf10'];
$vtf20=$_POST['vtf20'];
$vtf25=$_POST['vtf25'];
$vtf35=$_POST['vtf35'];
$vtf60=$_POST['vtf60'];
$vtf100=$_POST['vtf100'];
$subf10=$_POST['subf10'];
$subf25=$_POST['subf25'];

//variables venta unigas
$vuf10=$_POST['vuf10'];
$vuf20=$_POST['vuf20'];
$vuf25=$_POST['vuf25'];
$vuf35=$_POST['vuf35'];
$vuf60=$_POST['vuf60'];
$vuf100=$_POST['vuf100'];

//variables inventarios gas tomza
$vltf10=$_POST['ltf10'];
$vltf20=$_POST['ltf20'];
$vltf25=$_POST['ltf25'];
$vltf35=$_POST['ltf35'];
$vltf60=$_POST['ltf60'];
$vltf100=$_POST['ltf100'];

//variables inventarios gas unigas
$luf10=$_POST['luf10'];
$luf20=$_POST['luf20'];
$luf25=$_POST['luf25'];
$luf35=$_POST['luf35'];
$luf60=$_POST['luf60'];
$luf100=$_POST['luf100'];

//variables inventario cilindro vacio tomza
$ctf10=$_POST['ctf10'];
$ctf20=$_POST['ctf20'];
$ctf25=$_POST['ctf25'];
$ctf35=$_POST['ctf35'];
$ctf60=$_POST['ctf60'];
$ctf100=$_POST['ctf100'];

//variables inventario cilindro vacio unigas
$cuf10=$_POST['cuf10'];
$cuf20=$_POST['cuf20'];
$cuf25=$_POST['cuf25'];
$cuf35=$_POST['cuf35'];
$cuf60=$_POST['cuf60'];
$cuf100=$_POST['cuf100'];

//variables inventario cilindro vacio tropigas
$ctpf10=$_POST['ctpf10'];
$ctpf20=$_POST['ctpf20'];
$ctpf25=$_POST['ctpf25'];
$ctpf35=$_POST['ctpf35'];
$ctpf60=$_POST['ctpf60'];
$ctpf100=$_POST['ctpf100'];

//variables inventario cilindro vacio zgas
$czf10=$_POST['czf10'];
$czf20=$_POST['czf20'];
$czf25=$_POST['czf25'];
$czf35=$_POST['czf35'];
$czf60=$_POST['czf60'];
$czf100=$_POST['czf100'];

//inserta la venta TOMZA ---------->
$arrayventa=array(array('GT10', $vtf10, $subf10) ,array("GT20" , $vtf20,'0'),array("GT25" , $vtf25, $subf25), array("GT35" , $vtf35,'0'),array("GT60" , $vtf60,'0'), array("GT100" , $vtf100,'0'));

$table = 'vt_venta';  
foreach ($arrayventa as  $value) {
	$record = array();
 		$record["vt_id_tie"] = $tie_id;
		$record['vt_cantidad'] = $value[1];
		$cod_pro=$value[0];
		$cant_sub=$value[2];		
		
$sql = "SELECT * FROM pro_producto WHERE pro_cod='$cod_pro' ";
$result = $db->getrow($sql);
	$cantidad=$value[1];
	$galones=$cantidad*$result['pro_galon'];
	$montogalones=$cantidad*$result['pro_precio'];
	$monto_sub=$cant_sub*$result['pro_precio_sub'];
	$record["vt_id_pro"]  = $result['pro_id'];
	$record["vt_galones"]  = $galones;
	$record["vt_monto_gas"]  = $montogalones;
	$record["vt_cant_subsidios"]  = $cant_sub;
	$record["vt_monto_sub"]  = $monto_sub;
	$record["vt_total"]  = $montogalones-$monto_sub;

$db->autoExecute($table,$record,'INSERT');
}

//inserta la venta UNIGAS ---------->
$arrayventaU=array(array("GU20" , $vuf20,'0'),array("GU25" , $vuf25, '0'), array("GU35" , $vuf35,'0'),array("GU60" , $vuf60,'0'), array("GU100" , $vuf100,'0'));

$table = 'vt_venta';  
foreach ($arrayventaU as  $value) {
	$record = array();
 		$record["vt_id_tie"] = $tie_id;
		$record['vt_cantidad'] = $value[1];
		$cod_pro=$value[0];
		$cant_sub=$value[2];		
		
$sql = "SELECT * FROM pro_producto WHERE pro_cod='$cod_pro' ";
$result = $db->getrow($sql);
	$cantidad=$value[1];
	$galones=$cantidad*$result['pro_galon'];
	$montogalones=$cantidad*$result['pro_precio'];
	$monto_sub=$cant_sub*$result['pro_precio_sub'];
	$record["vt_id_pro"]  = $result['pro_id'];
	$record["vt_galones"]  = $galones;
	$record["vt_monto_gas"]  = $montogalones;
	$record["vt_cant_subsidios"]  = $cant_sub;
	$record["vt_monto_sub"]  = $monto_sub;
	$record["vt_total"]  = $montogalones-$monto_sub;

$db->autoExecute($table,$record,'INSERT');
}

//insert para agregar el inventario de gas tomza,cilindro vacio tomza, gas unigas, cilindro unigas,cilindro tropigas,zgas
$arraygastomza =array(array('GT10', $vltf10) ,array("GT20" , $vltf20),array("GT25" , $vltf25), array("GT35" , $vltf35),array("GT60" , $vltf60), array("GT100" , $vltf100),array("CT10", $ctf10),array("CT20", $ctf20),array("CT25", $ctf25),array("CT35", $ctf35),array("CT60", $ctf60),array("CT100", $ctf100),array("GU20" , $luf20),array("GU25" , $luf25), array("GU35" , $luf35),array("GU60" , $luf60), array("GU100" , $luf100),array("CU20" , $luf20),array("CU25" , $luf25), array("CU35" , $luf35),array("CU60" , $luf60), array("CU100" , $luf100),array('CTP10', $ctpf10) ,array("CTP20" , $ctpf20),array("CTP25" , $ctpf25), array("CTP35" , $ctpf35),array("CTP60" , $ctpf60), array("CTP100" , $ctpf100),array('CZ10', $czf10) ,array("CZ20" , $czf20),array("CZ25" , $czf25), array("CZ35" , $czf35),array("CZ60" , $czf60), array("CZ100" , $czf100));

$table = 'inv_inventario';  
foreach ($arraygastomza as  $value) {
	$record = array();
 		$record["inv_id_tie"] = $tie_id;
		$record['inv_cant'] = $value[1];
		$cod_pro=$value[0];
					
$sql = "SELECT * FROM pro_producto WHERE pro_cod='$cod_pro' ";
$result = $db->getrow($sql);	
	$record["inv_id_pro"]  = $result['pro_id'];	

$db->autoExecute($table,$record,'INSERT');
}
 
header("Location:index.php");
 ?>