<?php 
include("include/menu.php");
include('header.php');
require_once('funciones.php');
$link=conexioni();
$Id=$_GET['id'];

$st="SELECT tie_nombre,tie_id_canal,tie_stock,tie_moto,tie_vehiculo,tie_cilindros,tie_rotulacion,
tie_vendedor,tie_escritorio,tie_documentos,tie_tiene_telefono,tie_tiene_telegas,tie_encargado,tie_telefono,
tie_telefono_fijo,tie_id_sup,tie_presupuesto,tie_clasificacion,tie_vendedor1,tie_vendedor2,tie_vendedor3,tie_vendedor4,
tie_telegas,tie_telegas2,tie_monto_alquiler,tie_direccion,tie_id_municipio
 FROM tie_tienda	JOIN can_canal on canal_id=tie_id_canal	WHERE tie_id='$Id' LIMIT 1";
$rt=$connection->query($st);
$row  = $rt->fetch_assoc();
$tie_nombre=$row['tie_nombre'];
$canal_id=$row['tie_id_canal'];
$stocktienda=$row['tie_stock'];
$moto=$row['tie_moto'];
$vehiculo=$row['tie_vehiculo'];
$cilindros=$row['tie_cilindros'];
$rotulacion=$row['tie_rotulacion'];
$vendedor=$row['tie_vendedor'];
$escritorio=$row['tie_escritorio'];
$documentos=$row['tie_documentos'];
$telefono=$row['tie_tiene_telefono'];
$telegas=$row['tie_tiene_telegas'];
$encargadotienda=$row['tie_encargado'];
$numerotelefono=$row['tie_telefono'];
$numerotelefonofijo=$row['tie_telefono_fijo'];
$idsupervisor=$row['tie_id_sup'];
$presupuestotienda=$row['tie_presupuesto'];
$clasificaciontienda=$row['tie_clasificacion'];

$vendedor1=$row['tie_vendedor1'];
$vendedor2=$row['tie_vendedor2'];
$vendedor3=$row['tie_vendedor3'];
$vendedor4=$row['tie_vendedor4'];

$numerotelegas=$row['tie_telegas'];
$numerotelegas2=$row['tie_telegas2'];

$montoalquiler=$row['tie_monto_alquiler'];

$direcciontienda=$row['tie_direccion'];

$municipiotienda=$row['tie_id_municipio'];





     						
      					
?>
<section class="container" id="main">

<!-- Start Survey container -->
<div id="survey_container">

	<div id="top-wizard">
		<strong>Progreso <span id="location"></span></strong>
		<div id="progressbar"></div>
		<div class="shadow"></div>
	</div><!-- end top-wizard -->
    
	<form name="example-1" id="wrapped" action="updatetie.php" method="POST">
		<div id="middle-wizard">
			 <div class="btn-group">
   <button type="button" class="btn btn-warning" onclick="window.location.href='main.php'">regresar</button>
			</div>
			<div class="step">
				<div class="row">

					<h3 class="col-md-10">Estatus General de la tienda</h3>
					<div class="col-md-6">
						<ul class="data-list">
							<li><input type="text" name="nombretienda" class="required form-control" placeholder="Nombre de Tienda" value="<? echo $tie_nombre;?>"></li>
							<li><input type="number" name="stock" min="0" id="stock" class="required form-control" placeholder="Stock" value="<? echo $stocktienda;?>" ></li>
						</ul>
					</div><!-- end col-md-6 -->
                    
					<div class="col-md-6">
                    		<input type="hidden" name="idtie" id="idtie" value="<? echo $Id?>">
						<ul class="data-list">
							<li>
							<div class="styled-select">
								<select class="form-control required" name="canal" id="canal">									
                                        <option value="1" <?php if($canal_id==2){echo "selected";} ?>>Propia</option>
                                        <option value="2" <?php if($canal_id==3){echo "selected";} ?>>Mercantil</option>
                                 
								</select>
							</div>
							</li>
						</ul>
                        
						
                        
					</div><!-- end col-md-6 -->
					
				</div><!-- end row -->
				<div class="row">
				<div class="col-md-12">
						<ul class="data-list floated clearfix">
						<li><input name="moto" type="checkbox" class=" check_radio" value="1" <?php if($moto==1){echo "checked=checked";} ?>><label class="label_gender"> Moto</label></li>
							<li><input name="vehiculo" type="checkbox" class=" check_radio" value="1" <?php if($vehiculo==1){echo "checked=checked";} ?>><label class="label_gender">Vehiculo</label></li>
							<li><input name="cilindros" type="checkbox" class=" check_radio" value="1" <?php if($cilindros==1){echo "checked=checked";} ?>><label class="label_gender"> Cilindros</label></li>
							<li><input name="escritorio" type="checkbox" class=" check_radio" value="1" <?php if($escritorio==1){echo "checked=checked";} ?>><label class="label_gender"> Escritorio</label></li>
							<li><input name="telefono" type="checkbox" class=" check_radio" value="1" <?php if($telefono==1){echo "checked=checked";} ?>><label class="label_gender"> Telefono</label></li>
							<li><input name="rotulacion" type="checkbox" class=" check_radio" value="1" <?php if($rotulacion==1){echo "checked=checked";} ?>><label class="label_gender"> Rotulacion</label></li>
							<li><input name="documentos" type="checkbox" class=" check_radio" value="1" <?php if($documentos==1){echo "checked=checked";} ?>><label class="label_gender"> Documentos</label></li>
							<li><input name="telegas" type="checkbox" class=" check_radio" value="1" <?php if($telegas==1){echo "checked=checked";} ?>><label class="label_gender"> Telegas</label></li>
							<li><input name="vendedor" type="checkbox" class=" check_radio" value="1" <?php if($vendedor==1){echo "checked=checked";} ?>><label class="label_gender"> Vendedor</label></li>
							</ul>
					</div>
				</div>
                <div class="row">
					<div class="col-md-4 col-md-offset-4">
						
					</div>
				</div>
				
                
			</div><!-- end step-->
			<div class="step row">
				<div class="col-md-10">
					
					<div class="col-md-6">
					<h3>Datos Generales de la tienda.</h3>
					<ul class="data-list-2">
						<li><input name="nombreencargado" id="nombreencargado" type="text" class="required form-control" placeholder="Nombre de Encargado" value="<? echo $encargadotienda; ?>" ></li>
						<li><input name="telefonotienda" id="telefonotienda" type="text" class="required form-control" placeholder="Telefono de Tienda"  maxlength="8" onkeypress="return valida(event)" value="<? echo $numerotelefono; ?>"></li>
						<li><input name="telefonofijo" id="telefonofijo" type="text" maxlength="8" onkeypress="return valida(event)" class="form-control" placeholder="Telefono Fijo" value="<? echo $numerotelefonofijo; ?>"></li>
							<li>
							
								<select class="form-control required" name="supervisor" id="supervisor">
									<option value="" selected>Supervisor de la tienda</option>
									<?php
									$st="SELECT sup_id,sup_nombre FROM sup_supervisor WHERE sup_id_cedi='$idcedi'";
									$rt=$connection->query($st);
									while($row  = $rt->fetch_assoc()){
									
									?>
									
									<option value="<? echo $row['sup_id'] ?>" <?php if($row['sup_id']==$idsupervisor){echo "selected";} ?>> <? echo $row['sup_nombre'] ?> </option>
									<?php
									}
									?>
									
								</select>

							
	
							</li>
							<li>
							<input name="presupuesto" id="presupuesto" min="0" type="number" maxlength="8"  class="form-control" placeholder="Presupuesto" value='<? echo $presupuestotienda; ?>'>
							</li>
							
							
						</ul>
						<h3>Clasificación</h3>
						<ul class="data-list-2">
						<li><input name="clasificacion" type="radio" class="required check_radio" value="A" <?php if($clasificaciontienda=="A"){echo "checked=checked";} ?>><label>A</label></li>
						<li><input name="clasificacion" type="radio" class="required check_radio" value="B" <?php if($clasificaciontienda=="B"){echo "checked=checked";} ?>><label>B</label></li>
						<li><input name="clasificacion" type="radio" class="required check_radio" value="C" <?php if($clasificaciontienda=="C"){echo "checked=checked";} ?>><label>C</label></li>
						<!--<li><input name="clasificacion" type="radio" class="required check_radio" value="D"><label>D</label></li>-->
								</ul>
						</div>
						<div class="col-md-6">
						<h3>Informacion de Vehiculo</h3> 
						<ul class="data-list">						
							<? 
							$regs=traevehiculo1($Id,$link); 
							if(isset($regs)){
								
								//echo count($regs);
							}else {
									
								for ($i=1; $i <5; $i++) { 

									?>
						<li>
				<input name="placa<? echo $i;?>" id="placa<? echo $i;?>" type="text" class=" form-control" placeholder="Placa de vehiculo <? echo $i;?>" >
						</li>
						<li>
				<div class="styled-select">
								<select class="form-control " name="marca1" id="marca1">
									<option value="0" selected="">seleccione vehiculo</option>
									<?php
									$mc="SELECT marca_id,mc_nombre_marca,mc_tipo FROM mc_marcas  ORDER BY mc_tipo ASC";
									$rt=mysqli_query($link,$mc);
									while($row  = mysqli_fetch_array($rt)){
										if($row['2']==1){
											$tipov="Moto";
										}else if($row['2']==2){
											$tipov="Automovil";
										}else if($row['2']==3){
											$tipov="Camion";
										}
									
									?>
									<option value="<? echo $row['marca_id']?>"><? echo $row['mc_nombre_marca']." - ".$tipov;?></option>
									<?
									}
									?>									
								</select>
								</div>
								</li><br>
								<?}
							} ?>							
														
						</ul>
                        
					</div>
				</div>
			</div><!-- end step -->
            <div class="step row">
				<div class="col-md-10">
					<h3>Informacion de tienda.</h3>
					<ul class="data-list-2">
						<li><input name="nombremotociclista1" id="nombremotociclista1" type="text" class="form-control" placeholder="Motociclista 1" value="<? echo $vendedor1; ?>"></li>
						<li><input name="nombremotociclista2" id="nombremotociclista2" type="text" class="form-control" placeholder="Motociclista 2" value="<? echo $vendedor2; ?>"></li>
						<li><input name="nombremotociclista3" id="nombremotociclista3" type="text" class="form-control" placeholder="Motociclista 3" value="<? echo $vendedor3; ?>"></li>
						<li><input name="nombremotociclista4" id="nombremotociclista4" type="text" class="form-control" placeholder="Motociclista 4" value="<? echo $vendedor4; ?>"></li>
						<li><input name="telefonotelegas" id="telefonotelegas" type="text" maxlength="8" onkeypress="return valida(event)" class="form-control " placeholder="Telefono de Telegas" value="<? echo $numerotelegas; ?>"></li>
						<li><input name="telefonotelegas2" id="telefonotelegas2" type="text" maxlength="8" onkeypress="return valida(event)" class="form-control " placeholder="Telefono de Telegas 2" value="<? echo $numerotelegas2; ?>"></li>
						<h3>Alquiler</h3>
						<li><label class="switch-light switch-ios ">
                                    <input type="checkbox" name="alquiler" id="alquiler" class="fix_ie8" value="1" <? if($montoalquiler>0){echo "checked=checked";} ?> >
                                    <span>
                                        <span class="ie8_hide">No</span>
                                        <span>Si</span>
                                    </span>
                                    <a></a>
                                </label></li>
						
						<h3>Monto de Alquiler</h3>
						<li><input name="montoalquiler" id="montoalquiler" type="number" maxlength="6" class="number form-control" placeholder="Monto de alquiler" disabled="disabled" value="<? echo $montoalquiler; ?>"></li>
						<h3>Ubicacion</h3>
						<li><input name="direccion" id="direccion" type="text" class="form-control" placeholder="Direccion de la tienda" value="<? echo $direcciontienda; ?>" ></li>
						
						
							<h3>Municipio</h3>
							<li>							
								<select   class="form-control required" name="municipio" id="municipio">
									<option value="" selected>Municipio </option>
									<?php
									$sts="SELECT municipio,idmunicipio FROM municipios";
									$rts=$connection->query($sts);
									while($row  = $rts->fetch_assoc()){
									
									?>
									
									<option value="<? echo $row['idmunicipio'] ?>" <?php if($row['idmunicipio']==$municipiotienda){echo "selected";} ?>> <? echo $row['municipio'] ?> </option>
									<?php
									}
									?>
									
									
								</select>
							
							</li>
						
					</ul>
				
				
				
				</div>
			</div><!-- end step 4-->
			<div class="step row">
				<div class="col-md-12">
				<div id="owl-demo">
		
					<? traeimagen($Id,$link) ?>
		
					</div>
					<div class="col-md-6">
						<h3>Auditoria.</h3>
							<ul class="data-list-2">
								<li>
								
									<input name="image1" id="image1" type="file" class="form-control" placeholder="Auditoria">	
								</li>
								
						
							</ul>
					</div>
					<div class="col-md-6">
						<h3>Fachada.</h3>
							<ul class="data-list-2">
								<li>
								
									<input name="image2" id="image2" type="file" class="form-control" placeholder="Fachada">	
								</li>
								
						
							</ul>
					</div>
				
				
				
				</div>
				<div class="col-md-12">
				<div class="col-md-6">
						<h3>Vehiculo.</h3>
							<ul class="data-list-2">
								<li>
								
									<input name="image3" id="image3" type="file" class=" form-control" placeholder="Auditoria">	
								</li>
								
						
							</ul>
					</div>
				</div>
			</div><!-- end step -->
			<div class="submit step" id="complete">
                    	<i class="icon-check"></i>
						<h3>Gracias</h3>
						<button type="submit" name="process" class="submit">Guardar</button>
			</div><!-- end submit step -->
        
			<div id="bottom-wizard">
			<button type="button" name="backward" class="backward">Backward</button>
			<button type="button" name="forward" class="forward">Forward </button>
		</div><!-- end bottom-wizard -->

	</form>
    
</div><!-- end Survey container -->


</section><!-- end section main container -->
<!-- OTHER JS --> 
<script src="js/jquery.validate.js"></script>
<script src="js/jquery.placeholder.js"></script>
<script src="js/jquery.tweet.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/quantity-bt.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/retina.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/functions.js"></script>
<script>

document.getElementById("alquiler").onchange = function () {
      var c = document.getElementById('alquiler').value;
      if (this.value == '1'){
		document.getElementById("montoalquiler").removeAttribute("disabled");
	  }
        
    };
function setSelectValue (id, val) {
    document.getElementById(id).value = val;
}

function handleClick(myRadio) {
    alert('Old value: ' + currentValue);
    alert('New value: ' + myRadio.value);
    currentValue = myRadio.value;
}
</script>

<!-- FANCYBOX -->
<script  src="js/fancybox/source/jquery.fancybox.pack.js?v=2.1.4" type="text/javascript"></script> 
<script src="js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5" type="text/javascript"></script> 
<script src="js/fancy_func.js" type="text/javascript"></script> 

</body>
</html>
