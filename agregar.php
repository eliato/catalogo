<?php 
include("include/menu.php");
include('header.php');
  						
      					
?>

<section class="container" id="main">

<!-- Start Survey container -->
<div id="survey_container">

	<div id="top-wizard">
		<strong>Progreso <span id="location"></span></strong>
		<div id="progressbar"></div>
		<div class="shadow"></div>
	</div><!-- end top-wizard -->
    
	<form name="example-1" id="wrapped" action="agregartienda.php" method="POST" enctype="multipart/form-data">
		<div id="middle-wizard">
			<div class="step">
			<div class="btn-group">
   <button type="button" class="btn btn-warning" onclick="window.location.href='main.php'">regresar</button>
			</div>
				<div class="row">
				
					<h3 class="col-md-10">Estatus General de la tienda</h3>
					<div class="col-md-6">
					
						<ul class="data-list">
							<li><input type="text" name="nombretienda" id="nombretienda" class="required form-control" placeholder="Nombre de Tienda" ></li>
							<li><input type="number" name="stock" min="0" id="stock" class="required form-control" placeholder="Stock" ></li>
						</ul>
					</div><!-- end col-md-6 -->
                    
					<div class="col-md-6">
                    
						<ul class="data-list">
							<li>
							<div class="styled-select">
								<select class="form-control required" name="canal" id="canal">
										<option value="" >Seleccione el canal de la tienda</option>
                                        <option value="2" >Propia</option>
                                        <option value="3" >Mercantil</option>
                                 
								</select>
							</div>
							</li>
						</ul>
                        
						
                        
					</div><!-- end col-md-6 -->
					
				</div><!-- end row -->
				<div class="row">
				<div class="col-md-12">
						<ul class="data-list floated clearfix">
							<li><input name="moto" id="moto" type="checkbox" class="check_radio" value="1"><label class="label_gender"> Moto</label></li>
							<li><input name="vehiculo" id="vehiculo" type="checkbox" class="check_radio" value="1"><label class="label_gender">Vehiculo</label></li>
							<li><input name="cilindros" id="cilindros" type="checkbox" class="check_radio" value="1"><label class="label_gender"> Cilindros</label></li>
							<li><input name="escritorio" id="escritorio" type="checkbox" class="check_radio" value="1"><label class="label_gender"> Escritorio</label></li>
							<li><input name="telefono" id="telefono" type="checkbox" class="check_radio" value="1"><label class="label_gender"> Telefono</label></li>
							<li><input name="rotulacion" id="rotulacion" type="checkbox" class="check_radio" value="1"><label class="label_gender"> Rotulacion</label></li>
							<li><input name="documentos" id="documentos" type="checkbox" class="check_radio" value="1"><label class="label_gender"> Documentos</label></li>
							<li><input name="telegas" id="telegas" type="checkbox" class="check_radio" value="1"><label class="label_gender"> Telegas</label></li>
							<li><input name="vendedor" id="vendedor" type="checkbox" class="check_radio" value="1"><label class="label_gender"> Vendedor</label></li>
						</ul>
					</div>
				</div>
                <div class="row">
					<div class="col-md-4 col-md-offset-4">
						
					</div>
				</div>
				
                
			</div><!-- end step-->

			<div class="step row">
				<div class="col-md-10">
					
					<div class="col-md-6">
					<h3>Datos Generales de la tienda.</h3>
					<ul class="data-list-2">
						<li><input name="nombreencargado" id="nombreencargado" type="text" class="required form-control" placeholder="Nombre de Encargado" ></li>
						<li><input name="telefonotienda" id="telefonotienda" type="text" class="required form-control" placeholder="Telefono de Tienda"  maxlength="8" onkeypress="return valida(event)"></li>
						<li><input name="telefonofijo" id="telefonofijo" type="text" maxlength="8" onkeypress="return valida(event)" class="form-control" placeholder="Telefono Fijo"></li>
							<li>
							
								<select class="form-control required" name="supervisor" id="supervisor">
									<option value="" selected>Supervisor de la tienda</option>
									<?php
									$st="SELECT sup_id,sup_nombre FROM sup_supervisor WHERE sup_id_cedi='$idcedi'";
									$rt=$connection->query($st);
									while($row  = $rt->fetch_assoc()){
									
									?>
									
									<option value="<? echo $row['sup_id'] ?>"> <? echo $row['sup_nombre'] ?> </option>
									<?php
									}
									?>
									
								</select>

							
								<li><div class="btn-group">
					   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Añadir Supervisor</button>
								</div></li>
							</li>
							<li>
							<input name="presupuesto" id="presupuesto" min="0" type="number" maxlength="8"  class="form-control" placeholder="Presupuesto">
							</li>
							
							
						</ul>
						<h3>Clasificación</h3>
						<ul class="data-list-2">
						<li><input name="clasificacion" type="radio" class="required check_radio" value="A"><label>A</label></li>
						<li><input name="clasificacion" type="radio" class="required check_radio" value="B"><label>B</label></li>
						<li><input name="clasificacion" type="radio" class="required check_radio" value="C"><label>C</label></li>
						<!--<li><input name="clasificacion" type="radio" class="required check_radio" value="D"><label>D</label></li>-->
								</ul>
						</div>
						<div class="col-md-6">
						<h3>Informacion de Vehiculo</h3>
						<ul class="data-list">
						<li><input name="placa1" id="placa1" type="text" class="required form-control" placeholder="Placa de vehiculo 1"></li>
							<li>
							<div class="styled-select">
								<select class="form-control required" name="marca1" id="marca1">
									<option value="" selected>Marca de la moto </option>
									<?php
									$mc="SELECT marca_id,mc_nombre_marca,mc_tipo FROM mc_marcas ORDER BY mc_tipo ASC";
									$rt=$connection->query($mc);
									while($row  = $rt->fetch_assoc()){
										if($row['mc_tipo']==1){
											$tipov="Moto";

										}else if($row['mc_tipo']==2){
											$tipov="Automovil";
										}else if($row['mc_tipo']==3){
											$tipov="Camion";
										}
									
									?>
									<option value="<? echo $row['marca_id']?>"><? echo $row['mc_nombre_marca']." - ".$tipov;?></option>
									<?
									}
									?>
									
								</select>
							</div>
							</li>
							
							</li>
							
						</ul>
						<ul class="data-list">
							<li><input name="placa2" id="placa2" type="text" class=" form-control" placeholder="Placa de vehiculo 2"></li>
							<li>
							<div class="styled-select">
								<select class="form-control " name="marca2" id="marca2">
									<option value="" selected>Marca de la moto </option>
									<?php
									$mc="SELECT marca_id,mc_nombre_marca,mc_tipo FROM mc_marcas";
									$rt=$connection->query($mc);
									while($row  = $rt->fetch_assoc()){
										if($row['mc_tipo']==1){
											$tipov="Moto";

										}else if($row['mc_tipo']==2){
											$tipov="Automovil";
										}else if($row['mc_tipo']==3){
											$tipov="Camion";
										}
									
									?>
									<option value="<? echo $row['marca_id']?>"><? echo $row['mc_nombre_marca']." - ".$tipov;?></option>
									<?
									}
									?>
									
									
								</select>
							</div>
							</li>
							</ul>
						<ul class="data-list">
							<li><input name="placa3" id="placa3" type="text" class="form-control" placeholder="Placa de vehiculo 3"></li>
							<li>
							<div class="styled-select">
								<select class="form-control " name="marca3" id="marca3">
									<option value="" selected>Marca de la moto </option>
									<?php
									$mc="SELECT marca_id,mc_nombre_marca,mc_tipo FROM mc_marcas";
									$rt=$connection->query($mc);
									while($row  = $rt->fetch_assoc()){
										if($row['mc_tipo']==1){
											$tipov="Moto";

										}else if($row['mc_tipo']==2){
											$tipov="Automovil";
										}else if($row['mc_tipo']==3){
											$tipov="Camion";
										}
									
									?>
									<option value="<? echo $row['marca_id']?>"><? echo $row['mc_nombre_marca']." - ".$tipov;?></option>
									<?
									}
									?>
									
								</select>
							</div>
							</li>
							</ul>
							<ul class="data-list">
							<li><input name="placa4" id="placa4" type="text" class="form-control" placeholder="Placa de vehiculo 4"></li>
							<li>
							<div class="styled-select">
								<select class="form-control " name="marca4" id="marca4">
									<option value="" selected>Marca de la moto </option>
									<?php
									$mc="SELECT marca_id,mc_nombre_marca,mc_tipo FROM mc_marcas";
									$rt=$connection->query($mc);
									while($row  = $rt->fetch_assoc()){
										if($row['mc_tipo']==1){
											$tipov="Moto";

										}else if($row['mc_tipo']==2){
											$tipov="Automovil";
										}else if($row['mc_tipo']==3){
											$tipov="Camion";
										}
									
									?>
									<option value="<? echo $row['marca_id']?>"><? echo $row['mc_nombre_marca']." - ".$tipov;?></option>
									<?
									}
									?>
									
									
								</select>
							</div>
							</li>
							</ul>
						
						
                        
					</div>
				</div>
			</div><!-- end step -->
            <div class="step row">
				<div class="col-md-10">
					<h3>Informacion de tienda.</h3>
					<ul class="data-list-2">
						<li><input name="nombremotociclista1" id="nombremotociclista1" type="text" class="form-control" placeholder="Motociclista 1"></li>
						<li><input name="nombremotociclista2" id="nombremotociclista2" type="text" class="form-control" placeholder="Motociclista 2"></li>
						<li><input name="nombremotociclista3" id="nombremotociclista3" type="text" class="form-control" placeholder="Motociclista 3"></li>
						<li><input name="nombremotociclista4" id="nombremotociclista4" type="text" class="form-control" placeholder="Motociclista 4"></li>
						<li><input name="telefonotelegas" id="telefonotelegas" type="text" maxlength="8" onkeypress="return valida(event)" class="form-control " placeholder="Telefono de Telegas"></li>
						<li><input name="telefonotelegas2" id="telefonotelegas2" type="text" maxlength="8" onkeypress="return valida(event)" class="form-control " placeholder="Telefono de Telegas 2"></li>
						<h3>Alquiler</h3>
						<li><label class="switch-light switch-ios ">
                                    <input type="checkbox" name="alquiler" id="alquiler" class="fix_ie8" value="1">
                                    <span>
                                        <span class="ie8_hide">No</span>
                                        <span>Si</span>
                                    </span>
                                    <a></a>
                                </label></li>
						
						<h3>Monto de Alquiler</h3>
						<li><input name="montoalquiler" id="montoalquiler" type="number" maxlength="6" class="number form-control" placeholder="Monto de alquiler" disabled="disabled"></li>
						<h3>Ubicacion</h3>
						<li><input name="direccion" id="direccion" type="text" class="form-control" placeholder="Direccion de la tienda"></li>
						
						
							<h3>Municipio</h3>
							<li>							
								<select   class="form-control required" name="municipio" id="municipio">
									<option value="" selected>Municipio </option>
									<?php
									$sts="SELECT municipio,idmunicipio FROM municipios";
									$rts=$connection->query($sts);
									while($row  = $rts->fetch_assoc()){
									
									?>
									
									<option value="<? echo $row['idmunicipio'] ?>"> <? echo $row['municipio'] ?> </option>
									<?php
									}
									?>
									
									
								</select>
							
							</li>
						
					</ul>
				
				
				
				</div>
			</div><!-- end step 4-->
			<div class="step row">
				<div class="col-md-12">
					
					<div class="col-md-6">
						<h3>Auditoria.</h3>
							<ul class="data-list-2">
								<li>
								
									<input name="image1" id="image1" type="file" class="form-control" placeholder="Auditoria">	
								</li>
								
						
							</ul>
					</div>
					<div class="col-md-6">
						<h3>Fachada.</h3>
							<ul class="data-list-2">
								<li>
								
									<input name="image2" id="image2" type="file" class="form-control" placeholder="Fachada">	
								</li>
								
						
							</ul>
					</div>
				
				
				
				</div>
				<div class="col-md-12">
				<div class="col-md-6">
						<h3>Vehiculo.</h3>
							<ul class="data-list-2">
								<li>
								
									<input name="image3" id="image3" type="file" class=" form-control" placeholder="Auditoria">	
								</li>
								
						
							</ul>
					</div>
				</div>
			</div><!-- end step -->
			<div class="submit step" id="complete">
                    	<i class="icon-check"></i>
						<h3>Gracias</h3>
						<button type="submit" name="process" class="submit">Guardar</button>
			</div><!-- end submit step -->
        
			<div id="bottom-wizard">
			<button type="button" name="backward" class="backward">Backward</button>
			<button type="button" name="forward" class="forward">Forward </button>
		</div><!-- end bottom-wizard -->

	</form>
    
</div><!-- end Survey container -->

	
                
</section><!-- end section main container -->

<!-- Modal About -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Agregar Supervisor</h4>
      </div>
     <form id="form" action="addsup.php" method="post">
      <div class="modal-body">
      	<div id="error1" style="display:none;" ></div>
        <div id="error2" style="display:none;" ></div>
        <input name="nombresup" id="nombresup"  type="text"   class="form-control" placeholder="Nombre">
        <input name="telsup" id="telsup"  type="text"   class="form-control" placeholder="Telefono" maxlength="8" onkeypress="return valida(event)">
 		<input name="correosup" id="correosup"  type="email"   class="form-control" placeholder="Correo">
      </div>
      </form>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" onClick="validar();">Guardar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
document.getElementById("alquiler").onchange = function () {
      var c = document.getElementById('alquiler').value;
      if (this.value == '1'){
		document.getElementById("montoalquiler").removeAttribute("disabled");
	  }
        
    };

     $(document).ready(function() { $("#municipio").select2(); });
     $(document).ready(function() { $("#supervisor").select2(); });


function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}



  function validar(){
  	  if (!$("#nombresup").val()) {
          $("#error1").text("Ingrese nombre ").addClass("alert alert-danger").fadeIn("Slow");
          $('#username').focus();
          
          return false;
        }else if(!$("#telsup").val()) {
          $("#error2").text("ingrese Telefono").addClass("alert alert-success").fadeIn("Slow");
          $('#password').focus();
          return false;
        } else {
        	$('#form').submit();
        }
      

      }
</script>

<?php  

include('foother.php');
?>